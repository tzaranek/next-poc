import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider, createTheme, withStyles, Arwes, Puffs, Words, SoundsProvider, withSounds, createSounds  } from 'arwes';

const Player = withSounds()(props => (
    <button
        style={{ margin: 10 }}
        onClick={() => props.sounds[props.id].play()}
    >
        Play {props.id}
    </button>
));

const sounds = {
    shared: { volume: 10 },
    players: {
        information: { sound: { src: ['/static/sound/information.mp3'] } },
        ask: { sound: { src: ['/static/sound/warning.mp3'] } },
        warning: { sound: { src: ['/static/sound/warning.mp3'] } },
        error: { sound: { src: ['/static/sound/error.mp3'] } },
    },
};

const styles = theme => ({
  root: {
    padding: [theme.padding, 0],
    background: theme.background.primary.level0
  },
  title: {
    textDecoration: 'underline'
  }
});

const MyHeader = withStyles(styles)(({ classes, children }) => (
  <header className={classes.root}>
    <h1 className={classes.title}>{children}</h1>
  </header>
));

export default () => (
<div>
 <ThemeProvider theme={createTheme()}>
	        <SoundsProvider sounds={createSounds(sounds)}>
            <div>
                <Player id='information' />
                <Player id='ask' />
                <Player id='warning' />
                <Player id='error' />
            </div>
        </SoundsProvider>
  </ThemeProvider>
  </div>
)
